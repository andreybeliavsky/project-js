'use strict';

let personalMovieDB = {
    count: 0,
    movies: {},
    actors: {},
    genres: [],
    privat: false,
    start: function () {
        do {
            personalMovieDB.count = +prompt('Сколько фильмов вы уже посмотрели?', '');
        } while (personalMovieDB.count == null || personalMovieDB.count == '' || isNaN(personalMovieDB.count))
    },
    writeYourMovies: function () {
        for (let i = 0; i < 2; i++) {
            let a = prompt('Один из последних просмотренных фильмов?'),
                b = +prompt('На сколько оцените его?');

            if (a != null && b != null && a != '' && b != '' && a.length < 50 && isNaN(b) == false) {
                personalMovieDB.movies[a] = b;
                console.log('done');
            } else {
                console.log('error');
                i--;
            }
        }
    },
    writeYourGenres: function () {
        for (let i = 0; i < 3; i++) {
            let genre = prompt(`Ваш любимый жанр под номером ${i + 1}`);

            if (genre != null || genre != '') {
                personalMovieDB.genres[i] = genre;
                console.log('done');
            } else {
                console.log('error');
                i--;
            }
        }

        personalMovieDB.genres.forEach((item, i) => {
            console.log(`Любимый жанр ${i + 1} - это ${item}`);
        });
    },
    showPersonalLevel: function () {
        if (personalMovieDB.count <= 10) {
            console.log("Просмотрено довольно мало фильмов");
        } else if (personalMovieDB.count > 10 && personalMovieDB.count < 30) {
            console.log("Вы классический зритель");
        } else if (personalMovieDB.count >= 30) {
            console.log("Вы киноман");
        } else {
            console.log("error");
        }
    },
    showMyDB: function () {
        if (!personalMovieDB.privat) {
            console.log(personalMovieDB);
        }
    },
    toggleVisibleMyDB: function () {
        let toggle = personalMovieDB.privat ? personalMovieDB.privat = false : personalMovieDB.privat = true;
    }
};